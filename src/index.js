import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

function Square (props) {
  return (
    <button className={`square ${props.isWinning ? 'winning' : ''}`} onClick={props.onClick}>
      {props.value}
    </button>
  );
}

function Board (props) {
  const renderSquare = function (i) {
    return (
      <Square
        value={props.squares[i]}
        isWinning={props.winningSquares.has(i)}
        onClick={() => props.onClick(i)}
      />
    );
  };

  return (
    <div>
      {
        [0, 1, 2].map((_, row) => (
          <div className='board-row'>
            {
              [0, 1, 2].map((_, col) => renderSquare(row * 3 + col))
            }
          </div>
        ))
      }
    </div>
  );
}

class Game extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      history: [{
        squares: Array(9).fill(null),
        playedSquare: null
      }],
      xIsNext: true,
      stepNumber: 0,
      selectedHistoryStep: null,
      moveOrderAsc: true
    };
  }

  handleClick (i) {
    const history = this.state.history.slice(0, this.state.stepNumber + 1);
    const current = history[history.length - 1];
    const squares = [...current.squares];
    const { winner } = calculateWinner(squares);
    if (winner || squares[i]) {
      return;
    }

    squares[i] = getNextPlayer(this.state.xIsNext);
    this.setState({
      history: [...history, { squares, playedSquare: i }],
      stepNumber: history.length,
      xIsNext: !this.state.xIsNext,
      selectedHistoryStep: null
    });
  }

  jumpTo (step) {
    this.setState({
      stepNumber: step,
      xIsNext: (step % 2) === 0,
      selectedHistoryStep: step
    });
  }

  changeMoveOrder () {
    this.setState({
      moveOrderAsc: !this.state.moveOrderAsc
    });
  }

  render () {
    let history = [...this.state.history];
    const current = history[this.state.stepNumber];
    if (!this.state.moveOrderAsc) {
      history = history.reverse();
    }

    const selectedHistoryStep = this.state.selectedHistoryStep;
    const { winningSquares, winner } = calculateWinner(current.squares);
    const nextPlayer = getNextPlayer(this.state.xIsNext);
    const isDraw = this.state.stepNumber === 9;
    const status = getPlayerStatus(winner, isDraw, nextPlayer);
    const orderByLabel = this.state.moveOrderAsc ? 'desc' : 'asc';

    const moves = history.map((_, move) => {
      let desc = 'Go to game start';
      if (_.playedSquare !== null) {
        const coordinates = getSquareCoordinates(_.playedSquare);
        desc = `Go to move #${move} (${coordinates.x}, ${coordinates.y})`;
      }

      return (
        <li>
          <button key={move} className={selectedHistoryStep === move && 'selected-history'} onClick={() => this.jumpTo(move)}>
            {desc}
          </button>
        </li>
      );
    });

    return (
      <div className='game'>
        <div className='game-board'>
          <Board
            squares={current.squares}
            winningSquares={winningSquares}
            onClick={(i) => this.handleClick(i)} />
        </div>
        <div className='game-info'>
          <div>{status}</div>
          <button onClick={() => this.changeMoveOrder()}>order by {orderByLabel}</button>
          <ol>{moves}</ol>
        </div>
      </div>
    );
  }
}

function getSquareCoordinates (square) {
  return {
    x: square % 3,
    y: Math.floor(square / 3)
  };
}

function calculateWinner (squares) {
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6]
  ];

  for (let i = 0; i < lines.length; i++) {
    const [a, b, c] = lines[i];
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      return {
        winningSquares: new Set([a, b, c]),
        winner: squares[a]
      };
    }
  }

  return {
    winningSquares: new Set(),
    winner: null
  };
}

function getPlayerStatus (winner, isDraw, nextPlayer) {
  if (winner) {
    return `Winner: ${winner}`;
  }

  if (isDraw) {
    return `No winner :/ it's a draw !`;
  }

  return `Next player: ${nextPlayer}`;
}

function getNextPlayer (xIsNext) {
  return xIsNext ? 'X' : 'O';
}

// ========================================

ReactDOM.render(<Game />, document.getElementById('root'));
